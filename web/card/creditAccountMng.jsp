<%@ page import="com.fanap.model.customer.NaturalCustomers" %>
<%@ page import="com.fanap.model.card.CardAccountGroup" %>
<%@ page import="java.util.List" %>
<%@ page import="com.fanap.model.card.CreditProfile" %>
<%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/14/20
  Time: 6:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Card</title>
    <style>
        input[type=search] {
            width: 30%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            background-color: #f8f8f8;
        }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body style="text-align: center">
    <%
        String message = (String) request.getAttribute("message");
        if (message == null) {
            NaturalCustomers customer = (NaturalCustomers) request.getAttribute("customerByCustomerNumber");
            request.setAttribute("customerNumber", customer.getCustomerNumber());
            List<CardAccountGroup> cardAccountGroupList = (List<CardAccountGroup>) request.getAttribute("cardAccountGroupList");
    %>
    <table class="table table-condensed">
        <thead title="Customer Detail">
        <tr style="padding: 10px">
            <th>Customer Number</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Father Name</th>
            <th>Date of Birth</th>
            <th>National Code</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><%= customer.getCustomerNumber() %></td>
            <td><%= customer.getFirstName() %></td>
            <td><%= customer.getLastName() %></td>
            <td><%= customer.getFatherName() %></td>
            <td><%= customer.getDateOfBirth() %></td>
            <td><%= customer.getNationalCode() %></td>
        </tr>
        </tbody>
    </table>

    <form method="post" action="cardAccount?customerNumber=<%=customer.getCustomerNumber()%>">
        Choose a Card Account Group:
        <select name="cagId">
            <%
                for (CardAccountGroup cardAccountGroup : cardAccountGroupList) {
                    if (cardAccountGroup.getCreditProfile() != null) {
            %>
            <option value="<%=cardAccountGroup.getId()%>"><%=cardAccountGroup.getDescription()%> - <%=cardAccountGroup.getCreditProfile().getDescription()%></option>
            <%
            } else {
            %>
            <option value="<%=cardAccountGroup.getId()%>"><%=cardAccountGroup.getDescription()%> - null</option>
            <%
                    }
                }
            %>
        </select>
        <input type="submit" value="Request Card Account">
    </form>
    <form method="post" action="creditProfileMng?customerNumber=<%=customer.getCustomerNumber()%>">
        <button type="submit" >Credit Profile Management Page</button>
    </form>
    <form method="post" action="cardAccountGroupMng?customerNumber=<%=customer.getCustomerNumber()%>&">
        <button type="submit" >Card Account Group Management Page</button>
    </form>
    <%
    } else {
    %>
    <div style="text-align: center">
        <h1><%=message%></h1>
        <form action="cardAccountRequest?" method="post">
            <input type="search" name="customerNumber" placeholder="Try Again"> &nbsp;&nbsp;
            <button type="submit" value="card">Request Card</button>
        </form>
    </div>
    <%
        }
    %>
    <form method="post" action="customerType.jsp">
        <button style="margin-top: 8px" type="submit">Back</button>
    </form>
</body>
</html>