<%@ page import="com.fanap.model.customer.NaturalCustomers" %><%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/10/20
  Time: 4:18 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Result of Natural Customers' Search</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
    <%
        String message = (String) request.getAttribute("message");
        if (message == null) {
            NaturalCustomers customer = (NaturalCustomers) request.getAttribute("customerByCustomerNumber");
    %>
    <table class="table table-condensed">
        <thead>
        <caption><h2 style="text-align: center">A Natural Customer</h2></caption>
        <tr style="padding: 10px">
            <th>Customer Number</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Father Name</th>
            <th>Date of Birth</th>
            <th>National Code</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><%= customer.getCustomerNumber() %></td>
            <td><%= customer.getFirstName() %></td>
            <td><%= customer.getLastName() %></td>
            <td><%= customer.getFatherName() %></td>
            <td><%= customer.getDateOfBirth() %></td>
            <td><%= customer.getNationalCode() %></td>
            <td>
                <a href="deleteN?action=delete&customerNumber=<%=customer.getCustomerNumber()%>" onclick="return confirm('Are you sure to Delete this?')">Delete</a>&nbsp;&nbsp;&nbsp;
                <a href="updateN?action=update&customerNumber=<%=customer.getCustomerNumber()%>">Update</a>
            </td>
        </tr>
        </tbody>
    </table>
    <form method="post" action="retrieveAllN">
        <button style="margin-top: 8px" type="submit">Back to Customer Management</button>
    </form>
    <%
        } else {

    %>
    <div style="text-align: center">
        <h1><%=message%></h1>
        <form method="post" action="naturalCustomerRegister.jsp">
            New Customer? <button class="button" style="margin-top: 8px" type="submit">Register Account</button>
        </form>
        <form method="post" action="retrieveAllN">
            <button style="margin-top: 8px" type="submit">Back to Customer Management</button>
        </form>
    </div>
    <%
        }
    %>

</body>
</html>
