<%@ page import="com.fanap.model.customer.NaturalCustomers" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 11/28/20
  Time: 8:11 PM
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <title>Natural Customers</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body style="text-align: center;">
    <div style="border: aqua">
        <%
            List<NaturalCustomers> customers = (List<NaturalCustomers>) request.getAttribute("nListCustomer");
        %>
        <table class="table table-condensed">
            <thead>
                <caption><h2 style="text-align: center">List of Natural Customers</h2></caption>
                <tr style="padding: 10px">
                    <th>Customer Number</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Father Name</th>
                    <th>Date of Birth</th>
                    <th>National Code</th>
                </tr>
            </thead>
            <tbody>
                    <%
                        for (NaturalCustomers customer : customers) {
                    %>
                <tr>
                    <td><%= customer.getCustomerNumber() %></td>
                    <td><%= customer.getFirstName() %></td>
                    <td><%= customer.getLastName() %></td>
                    <td><%= customer.getFatherName() %></td>
                    <td><%= customer.getDateOfBirth() %></td>
                    <td><%= customer.getNationalCode() %></td>
                </tr>
            <%
                        }
            %>
            </tbody>
        </table>
    </div><br/>

    <form method="post" action="findN?">
        <label for="customerSearch">Search Customer: </label>
        <input type="search" id="customerSearch" name="customerNumber">
        <button type="submit" onclick="return isFindAccountEmpty()">Find Account</button>
    </form>

    <script>
        function isFindAccountEmpty() {
            var customerNumber = document.getElementById("customerSearch").value;
            if (customerNumber === "") {
                alert("Enter a Customer Number");
                return false;
            } else {
                return true;
            }
        }
    </script>

    <form method="post" action="naturalCustomerRegister.jsp">
        New Customer? <button class="button" style="margin-top: 8px" type="submit">Register Account</button>
    </form>
    <form method="post" action="customerType.jsp">
        Select User Type <button class="button" style="margin-top: 8px" type="submit">Customer Type</button>
    </form>
</body>
</html>