<%@ page import="com.fanap.model.customer.NaturalCustomers" %><%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/9/20
  Time: 3:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        input[type = text] {
            width: 30%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            background-color: #f8f8f8;
        }
    </style>
</head>
<body style="margin-outside: 50px;">
    <%
        NaturalCustomers customer = (NaturalCustomers) request.getAttribute("naturalCustomer");
    %>
    <form method="post" action="updateN">
        <label for="firstName">First Name </label><br/>
        <input type="text" id="firstName" name="firstName" placeholder="<%=customer.getFirstName()%>" onkeypress="isFirstNameString()"><br/>

        <label for="lastName">Last Name </label><br/>
        <input type="text" id="lastName" name="lastName" placeholder="<%=customer.getLastName()%>" onkeypress="isLastNameString()"><br/>

        <label for="fatherName">Father Name </label><br/>
        <input type="text" id="fatherName" name="fatherName" placeholder="<%=customer.getFatherName()%>" onkeypress="isFatherNameString()"><br/>

        <label for="dateOfBirth">Birthday Date </label><br/>
        <input type="text" id="dateOfBirth" name="dateOfBirth" placeholder="<%=customer.getDateOfBirth()%>"><br/>

        <label for="nationalCode">National Code </label><br/>
        <input type="text" id="nationalCode" name="nationalCode" placeholder="<%=customer.getNationalCode()%>"><br/>

        <button style="margin-top: 8px" type="submit">Update this Account</button>
    </form>
    <form method="post" action="findN?customerNumber=<%=customer.getCustomerNumber()%>">
        <button style="margin-top: 8px" type="submit">Back to Customer Management</button>
    </form>
    <script>
        function isFirstNameString(event) {
            var char = String.fromCharCode(event.which);
            if (!/[a-z A-Z]/.test(char)) {
                event.preventDefault();
                alert("First Name must only be letters");
            }
        }

        function isLastNameString(event) {
            var char = String.fromCharCode(event.which);
            if (!/[a-z A-Z ]/.test(char)) {
                event.preventDefault();
                alert("Last Name must only be letters");
            }
        }

        function isFatherNameString(event) {
            var char = String.fromCharCode(event.which);
            if (!/[a-z A-Z]/.test(char)) {
                event.preventDefault();
                alert("Father Name must only be letters");
            }
        }
    </script>
</body>
</html>
