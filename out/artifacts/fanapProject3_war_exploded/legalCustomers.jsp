<%@ page import="com.fanap.model.customer.LegalCustomers" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/1/20
  Time: 4:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Legal Customers List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body style="text-align: center;">
    <table class="table table-condensed">
        <thead>
        <caption><h2 style="text-align: center">List of Legal Customers</h2></caption>
        <tr style="padding: 10px">
            <th>Customer Number</th>
            <th>Company Name</th>
            <th>Date of Foundation</th>
            <th>Economy Code</th>
        </tr>
        </thead>
        <tbody>
        <%
            List<LegalCustomers> customers = (List<LegalCustomers>) request.getAttribute("lListCustomer");
            for (LegalCustomers customer : customers) {
        %>
        <tr>
            <td><%= customer.getCustomerNumber() %></td>
            <td><%= customer.getCompanyName() %></td>
            <td><%= customer.getDateOfFoundation() %></td>
            <td><%= customer.getEconomyCode() %></td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>
    <form method="post" action="findL?">
        <label for="customerSearch">Search Customer: </label>
        <input id="customerSearch" type="search" name="customerNumber">
        <button type="submit" onclick="return isFindAccountEmpty()">Find Account</button>
    </form>

    <form method="post" action="legalCustomerRegister.jsp">
        New Customer? <button class="button" style="margin-top: 8px" type="submit">Register Account</button>
    </form>
    <form method="post" action="customerType.jsp">
        Select User Type <button class="button" style="margin-top: 8px" type="submit">Customer Type</button>
    </form>
    <script>
        function isFindAccountEmpty() {
            var customerNumber = document.getElementById("customerSearch").value;
            if (customerNumber === "") {
                alert("Enter a Customer Number");
                return false;
            } else {
                return true;
            }
        }
    </script>
</body>
</html>
