<%@ page import="com.fanap.model.customer.LegalCustomers" %><%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/11/20
  Time: 6:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update Selected User</title>
    <style>
        input[type=text] {
            width: 30%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            background-color: #f8f8f8;
        }
    </style>
</head>
<body style="margin-outside: 50px;">
    <%
        LegalCustomers legalCustomer = (LegalCustomers) request.getAttribute("legalCustomer");
    %>
    <form method="post" action="updateL">
        <label for="companyName">Company Name: </label><br/>
        <input id="companyName" name="companyName" type="text" placeholder="<%=legalCustomer.getCompanyName()%>"><br/>

        <label for="companyDate">Date of Foundation: </label><br/>
        <input id="companyDate" name="companyDate" type="text" placeholder="<%=legalCustomer.getDateOfFoundation()%>"><br/>

        <label for="economyCode">Economy Code: </label><br/>
        <input id="economyCode" name="economyCode" type="text" placeholder="<%=legalCustomer.getEconomyCode()%>"><br/>

        <button type="submit">Update this Account</button>
    </form>
    <form method="post" action="findL?id=<%=legalCustomer.getCompanyId()%>">
        <button style="margin-top: 8px" type="submit">Back to Customer Management</button>
    </form>
</body>
</html>
