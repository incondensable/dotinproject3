<%@ page import="com.fanap.model.customer.LegalCustomers" %><%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/11/20
  Time: 5:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Result of Legal Customers' Search</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
    <%
        boolean check = (Boolean) request.getAttribute("check");
        if (check) {
            LegalCustomers legalCustomer = (LegalCustomers) request.getAttribute("customerById");
    %>
    <table class="table table-condensed">
        <thead>
            <tr>
                <th>Customer Number</th>
                <th>Company Name</th>
                <th>Date of Foundation</th>
                <th>Economy Code</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <tr>
            <td><%=legalCustomer.getCustomerNumber()%></td>
            <td><%=legalCustomer.getCompanyName()%></td>
            <td><%=legalCustomer.getDateOfFoundation()%></td>
            <td><%=legalCustomer.getEconomyCode()%></td>
            <td>
                <a href="deleteL?action=delete&customerNumber=<%=legalCustomer.getCustomerNumber()%>" onclick="return confirm('Are you sure to Delete this?')">Delete</a>&nbsp;&nbsp;&nbsp;
                <a href="updateL?action=update&customerNumber=<%=legalCustomer.getCustomerNumber()%>">Update</a>
            </td>
        </tr>
        </tbody>
    </table>
    <form method="post" action="retrieveAllL">
        <button type="submit" style="margin-top: 8px;">Back to Customer Management</button>
    </form>

    <%
    } else {
        String message = (String) request.getAttribute("message");
    %>
    <h1><%=message%></h1>
    <form method="post" action="retrieveAllL">
        <button type="submit" style="margin-top: 8px;">Back to Customer Management</button>
    </form>
    <form method="post" action="legalCustomerRegister.jsp">
        New Customer? <button class="button" style="margin-top: 8px" type="submit">Register Account</button>
    </form>
    <%
        }
    %>
</body>
</html>