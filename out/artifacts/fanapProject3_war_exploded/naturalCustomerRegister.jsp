<%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/7/20
  Time: 6:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create a New Account</title>
    <style>
        input[type=text] {
            width: 30%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            background-color: #f8f8f8;
        }
    </style>
</head>
<body style="margin-outside: 50px;">
    <h1>Create a new account</h1>
    <form name="registerForm" method="post" action="createN">
        <label for="first">First Name </label><br/>
        <input type="text" id="first" name="firstName" placeholder="eg: John" onkeypress="isFirstNameString(event)"><br/>
        <label for="last">Last Name </label><br/>
        <input type="text" id="last" name="lastName" placeholder="eg: Doe" onkeypress="isLastNameString(event)"><br/>
        <label for="father">Father Name </label><br/>
        <input type="text" id="father" name="fatherName" placeholder="eg: John's father" onkeypress="isFatherNameString(event)"><br/>
        <label for="dateOfBirth">Birthday Date </label><br/>
        <input type="text" id="dateOfBirth" name="dateOfBirth" placeholder="eg: 1990-12-20"><br/>
        <label for="nationalCode">National Code </label><br/>
        <input type="text" id="nationalCode" name="nationalCode" placeholder="eg: 1234567890" oninvalid="alert('National Code must be 10 numbers!')" pattern=".{10,10}"><br/>
        <button style="margin-top: 8px" type="submit">Create your Account</button> &nbsp;
    </form>
    <form method="post" action="retrieveAllN">
        <button style="margin-top: 8px" type="submit">Back to Customer Management</button>
    </form>
    <%
        String message = (String) request.getAttribute("message");
        if (message != null) {
    %>
    <script>alert('<%=message%>')</script>
    <form method="post" action="naturalCustomerRegister.jsp">
        New Customer? <button class="button" style="margin-top: 8px" type="submit">Register Account</button>
    </form>
    <%
        }
    %>
    <script>
        function isFirstNameString(event) {
            var char = String.fromCharCode(event.which);
            if (!/[a-z A-Z]/.test(char)) {
                event.preventDefault();
                alert("First Name must only be letters");
            }
        }
        function isLastNameString(event) {
            var char = String.fromCharCode(event.which);
            if (!/[a-z A-Z ]/.test(char)) {
                event.preventDefault();
                alert("Last Name must only be letters");
            }
        }
        function isFatherNameString(event) {
            var char = String.fromCharCode(event.which);
            if (!/[a-z A-Z]/.test(char)) {
                event.preventDefault();
                alert("Father Name must only be letters");
            }
        }
    </script>
</body>
</html>
