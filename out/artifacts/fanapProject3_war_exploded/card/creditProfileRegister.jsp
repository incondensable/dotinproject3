<%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/20/20
  Time: 3:54 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Credit Profile Register</title>
    <style>
        input[type=text] {
            width: 30%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            background-color: #f8f8f8;
        }
    </style>
</head>
<body>
    <%
        long customerNumber = (long) request.getAttribute("customerNumber");
    %>
    <form method="post" action="creditProfileC?customerNumber=<%=customerNumber%>">
        <label for="description">Credit Profile Description </label><br/>
        <input required type="text" id="description" name="description" placeholder="eg: Credit Profile A"><br/>
        <label for="minBound">Minimum Credit Bound</label><br/>
        <input type="text" id="minBound" name="minBound" placeholder="eg: 1000.0"><br/>
        <label for="maxBound">Maximum Credit Bound</label><br/>
        <input type="text" id="maxBound" name="maxBound" placeholder="eg: 20000.0"><br/>
        <button style="margin-top: 8px" type="submit" onclick="return evaluateBounds()">Create your Account</button>
    </form>
    <form method="post" action="creditProfileMng?customerNumber=<%=customerNumber%>">
        <button type="submit">Back</button>
    </form>

<script>
    function evaluateBounds() {
        var min = parseFloat(document.getElementById('minBound').value);
        var max = parseFloat(document.getElementById('maxBound').value);

        if (isNaN(min) || isNaN(max)) {
            alert("Entered values are not numeric.");
            return false;
        }

        if (min > max) {
            alert("Maximum Value must be Bigger than Minimum Value!");
            return false;
        }
        return true;
    }
</script>
</body>
</html>
