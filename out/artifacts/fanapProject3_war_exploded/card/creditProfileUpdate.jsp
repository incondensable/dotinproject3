<%@ page import="com.fanap.model.card.CreditProfile" %><%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/21/20
  Time: 12:41 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Credit Profile Update</title>
    <style>
        input[type=text] {
            width: 30%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            background-color: #f8f8f8;
        }
    </style>
</head>
<body>
    <%
        CreditProfile creditProfile = (CreditProfile) request.getAttribute("creditProfile");
        long customerNumber = (long) request.getAttribute("customerNumber");
    %>
    <form method="post" action="creditProfileUpd" class="form-container">
        <label for="creditProfileDesc"><b>Credit Profile Description</b></label><br/>
        <input id="creditProfileDesc" type="text" placeholder="<%=creditProfile.getDescription()%>" name="creditProfileDesc" required><br/>

        <label for="creditProfileMinimumBound"><b>Credit Profile Minimum Bound</b></label><br/>
        <input id="creditProfileMinimumBound" type="text" placeholder="<%=creditProfile.getMinCreditBound()%>" name="creditProfileMinimumBound"><br/>

        <label for="creditProfileMaximumBound"><b>Credit Profile Maximum Bound</b></label><br/>
        <input id="creditProfileMaximumBound" type="text" placeholder="<%=creditProfile.getMaxCreditBound()%>" name="creditProfileMaximumBound"><br/>

        <button type="submit" class="btn">Update</button>
    </form>
    <form method="post" action="creditProfileMng?customerNumber=<%=customerNumber%>">
        <button type="submit">Back</button>
    </form>
</body>
</html>
