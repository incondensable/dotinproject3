<%@ page import="java.util.List" %>
<%@ page import="com.fanap.model.card.CardAccountGroup" %><%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/21/20
  Time: 12:03 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Card Account Group Management</title>
    <style>
        input[type=search] {
            width: 30%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            background-color: #f8f8f8;
        }

        .customButton {
            box-shadow: inset 0px 1px 0px 0px #ffffff;
            background: #ededed linear-gradient(to bottom, #ededed 5%, #dfdfdf 100%);
            border-radius: 6px;
            border: 1px solid #dcdcdc;
            display: inline-block;
            cursor: pointer;
            color: #787878;
            font-family: Arial;
            font-size: 15px;
            font-weight: bold;
            padding: 6px 24px;
            text-decoration: none;
            text-shadow: 0px 1px 0px #ffffff;
        }

        .customButton:hover {
            background: #dfdfdf linear-gradient(to bottom, #dfdfdf 5%, #ededed 100%);
        }

        .customButton:active {
            position: relative;
            top: 1px;
        }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<%
    List<CardAccountGroup> cardAccountGroupList = (List<CardAccountGroup>) request.getAttribute("cardAccountGroupList");
    long customerNumber = (long) request.getAttribute("customerNumber");
%>
<table class="table table-condensed">
    <%
        String message = (String) request.getAttribute("message");
        if (message == null) {
    %>
    <thead>
    <tr>
        <th>Description</th>
        <th>Credit Profile Description</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <%
        for (CardAccountGroup cardAccountGroup : cardAccountGroupList) {
    %>
    <tr>
        <td><%=cardAccountGroup.getDescription()%>
        </td>
        <%
            if (cardAccountGroup.getCreditProfile() != null) {
        %>
        <td><%=cardAccountGroup.getCreditProfile().getDescription()%>
        </td>
        <%
        } else {
        %>
        <td>null</td>
        <%
            }
        %>
        <td>
            <%
                if (cardAccountGroup.getCreditProfile() != null) {
            %>
            <a href="cardAccountGroupUpd?customerNumber=<%=customerNumber%>&cardAccountGroupId=<%=cardAccountGroup.getId()%>&creditProfileId=<%=cardAccountGroup.getCreditProfile().getId()%>"
               class="customButton">Update</a>
            <%
            } else {
            %>
            <a href="cardAccountGroupUpd?customerNumber=<%=customerNumber%>&cardAccountGroupId=<%=cardAccountGroup.getId()%>&creditProfileId=null"
               class="customButton">Update</a>
            <%
                }
            %>
            <a href="cardAccountGroupDel?customerNumber=<%=customerNumber%>&cardAccountGroupId=<%=cardAccountGroup.getId()%>"
               class="customButton">Delete</a>
        </td>
    </tr>
    <%
        }
    %>
    </tbody>
</table>
<form method="post" action="cardAccountGroupC?action=post&customerNumber=<%=customerNumber%>">
    <button type="submit">Create a new Card Account Group</button>
</form>
<form method="post" action="cardAccountRequest?customerNumber=<%=customerNumber%>">
    <button type="submit">Back</button>
</form>
    <%
    } else {
    %>
    <h3><%=message%></h3>
    <form method="post" action="cardAccountGroupMng?customerNumber=<%=customerNumber%>">
        <button type="submit">Back to Card Account Group Management</button>
    </form>
    <%
        }
    %>
</body>
</html>