<%@ page import="com.fanap.model.card.CardAccountGroup" %>
<%@ page import="com.fanap.model.card.CreditProfile" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/21/20
  Time: 1:57 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Card Account Group Update</title>
    <style>
        input[type=text] {
            width: 30%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            background-color: #f8f8f8;
        }
    </style>
</head>
<body>
<%
    CardAccountGroup cardAccountGroup = (CardAccountGroup) request.getAttribute("cardAccountGroup");
    long customerNumber = (long) request.getAttribute("customerNumber");
    CreditProfile creditProfile = (CreditProfile) request.getAttribute("creditProfile");
    List<CreditProfile> creditProfileList = (List<CreditProfile>) request.getAttribute("creditProfileList");
%>
<form method="post" action="cardAccountGroupUpd" class="form-container">
    <label for="cardAccountGroupDescription"><b>Credit Profile Description</b></label><br/>
    <input id="cardAccountGroupDescription" type="text" placeholder="<%=cardAccountGroup.getDescription()%>"
           name="cardAccountGroupDescription" required><br/>

    <select name="creditProfileId">
        <%
            for (CreditProfile cp : creditProfileList) {
                if (creditProfile != null) {
                    if (!cp.getDescription().equals(creditProfile.getDescription())) {
        %>
        <option value="<%=cp.getId()%>"><%=cp.getDescription()%> - <%=cp.getMinCreditBound()%>
            - <%=cp.getMaxCreditBound()%>
        </option>
        <%
        } else {
        %>
        <option selected value="<%=cp.getId()%>"><%=cp.getDescription()%> - <%=cp.getMinCreditBound()%>
            - <%=cp.getMaxCreditBound()%>
        </option>
        <%
            }
        } else {
        %>
        <option value="<%=cp.getId()%>"><%=cp.getDescription()%> - <%=cp.getMinCreditBound()%>
            - <%=cp.getMaxCreditBound()%>
        </option>
        <%
                }
            }
        %>
    </select><br/>
    <button type="submit" class="btn">Update</button>
</form>
<form method="post" action="cardAccountGroupMng?customerNumber=<%=customerNumber%>">
    <button type="submit">Back</button>
</form>
</body>
</html>
