<%@ page import="com.fanap.model.card.CardAccountGroup" %>
<%@ page import="com.fanap.model.card.CardAccount" %><%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/18/20
  Time: 5:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Card Account</title>
    <style>
        input[type=search] {
            width: 30%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            background-color: #f8f8f8;
        }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
    <%
        CardAccount cardAccount = (CardAccount) request.getAttribute("cardAccount");
        long customerNumber = (Long) request.getAttribute("customerNumber");
    %>
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>Date</th>
            <th>File Number</th>
            <th>Card Account Group Description</th>
            <th>Credit Profile Description</th>
            <th>Minimum Bound</th>
            <th>Maximum Bound</th>
            <th>Customer</th>
            <th>First Name</th>
            <th>Last Name</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><%=cardAccount.getCreationDate()%></td>
            <td><%=cardAccount.getFileNumber()%></td>
            <td><%=cardAccount.getCardAccountGroup().getDescription()%></td>
            <td><%=cardAccount.getCardAccountGroup().getCreditProfile().getDescription()%></td>
            <td><%=cardAccount.getCardAccountGroup().getCreditProfile().getMinCreditBound()%></td>
            <td><%=cardAccount.getCardAccountGroup().getCreditProfile().getMaxCreditBound()%></td>
            <td><%=cardAccount.getNaturalCustomers().getCustomerNumber()%></td>
            <td><%=cardAccount.getNaturalCustomers().getFirstName()%></td>
            <td><%=cardAccount.getNaturalCustomers().getLastName()%></td>
        </tr>
        </tbody>
    </table>

    <form method="post" action="cardAccountRequest?customerNumber=<%=customerNumber%>">
        <button type="submit" value="back">Back to Card Account Request Page</button>
    </form>
</body>
</html>
