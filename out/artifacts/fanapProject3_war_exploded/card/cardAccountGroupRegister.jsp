<%@ page import="com.fanap.model.card.CreditProfile" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/20/20
  Time: 4:08 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Card Account Group Register</title>
    <style>
        input[type=text] {
            width: 30%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            background-color: #f8f8f8;
        }
    </style>
</head>
<body>
    <%
        List<CreditProfile> creditProfileList = (List<CreditProfile>) request.getAttribute("creditProfileList");
        long customerNumber = (long) request.getAttribute("customerNumber");
    %>
    <form method="post" action="cardAccountGroupC?customerNumber=<%=customerNumber%>">
        <label for="description">Card Account Group Description </label><br/>
        <input required type="text" id="description" name="description" placeholder="eg: Card Account Group A"><br/>
        <select name="creditProfileId">
            <%
                for (CreditProfile creditProfile : creditProfileList) {
            %>
            <option value="<%=creditProfile.getId()%>"><%=creditProfile.getDescription()%> - <%=creditProfile.getMinCreditBound()%> - <%=creditProfile.getMaxCreditBound()%></option>
            <%
                }
            %>
        </select>
        <button style="margin-top: 8px" type="submit">Create your Account</button>
    </form>
    <form method="post" action="cardAccountGroupMng?customerNumber=<%=customerNumber%>">
        <button type="submit">Back</button>
    </form>
</body>
</html>
