<%@ page import="com.fanap.model.card.CreditProfile" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/20/20
  Time: 11:49 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        input[type=search] {
            width: 30%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            background-color: #f8f8f8;
        }

        .customButton {
            box-shadow: inset 0px 1px 0px 0px #ffffff;
            background: #ededed linear-gradient(to bottom, #ededed 5%, #dfdfdf 100%);
            border-radius: 6px;
            border: 1px solid #dcdcdc;
            display: inline-block;
            cursor: pointer;
            color: #787878;
            font-family: Arial;
            font-size: 15px;
            font-weight: bold;
            padding: 6px 24px;
            text-decoration: none;
            text-shadow: 0px 1px 0px #ffffff;
        }

        .customButton:hover {
            background: #dfdfdf linear-gradient(to bottom, #dfdfdf 5%, #ededed 100%);
        }

        .customButton:active {
            position: relative;
            top: 1px;
        }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<%
    List<CreditProfile> creditProfileList = (List<CreditProfile>) request.getAttribute("creditProfileList");
    long customerNumber = (long) request.getAttribute("customerNumber");
    String message = (String) request.getAttribute("message");
    if (message == null) {
%>
<table class="table table-condensed">
    <thead>
    <tr>
        <th>Description</th>
        <th>Minimum Bound</th>
        <th>Maximum Bound</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <%
        for (CreditProfile creditProfile : creditProfileList) {
    %>
    <tr>
        <td><%=creditProfile.getDescription()%>
        </td>
        <td><%=creditProfile.getMinCreditBound()%>
        </td>
        <td><%=creditProfile.getMaxCreditBound()%>
        </td>
        <td>
            <a href="creditProfileUpd?customerNumber=<%=customerNumber%>&creditProfileId=<%=creditProfile.getId()%>"
               class="customButton">Update</a>
            <a href="creditProfileDel?customerNumber=<%=customerNumber%>&creditProfileId=<%=creditProfile.getId()%>"
               class="customButton">Delete</a>
        </td>
    </tr>
    <%
        }
    %>
    </tbody>
</table>
<form method="post" action="creditProfileC?action=post&customerNumber=<%=customerNumber%>">
    <button type="submit">Create a new Credit Profile</button>
</form>
<form method="post" action="cardAccountRequest?customerNumber=<%=customerNumber%>">
    <button type="submit">Back</button>
</form>
<%
} else {
%>
<h3><%=message%></h3>
<form method="post" action="creditProfileMng?customerNumber=<%=customerNumber%>">
    <button type="submit">Back to Credit Profile Management</button>
</form>
<%
    }
%>
</body>
</html>
