<%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 12/11/20
  Time: 6:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        input[type=text] {
            width: 30%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            background-color: #f8f8f8;
        }
    </style>
</head>
<body>
    <h1>Create a new account</h1>
    <form method="post" action="createL">
        <label for="companyName">First Name </label><br/>
        <input type="text" id="companyName" name="companyName" placeholder="eg: Fanap"><br/>

        <label for="companyDate">Last Name </label><br/>
        <input type="text" id="companyDate" name="companyDate" placeholder="eg: 1990-12-20"><br/>

        <label for="economyCode">Father Name </label><br/>
        <input type="text" id="economyCode" name="economyCode" placeholder="eg: 123456789012" oninvalid="alert('Economy Code must be 12 numbers!')" pattern=".{12,12}"><br/>

        <button style="margin-top: 8px" type="submit">Create your Account</button> &nbsp;
    </form>
    <form method="post" action="retrieveAllL">
        <button style="margin-top: 8px" type="submit">Back to Customer Management</button>
    </form>
</body>
</html>