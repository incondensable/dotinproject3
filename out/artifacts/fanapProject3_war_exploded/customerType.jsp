<%--
  Created by IntelliJ IDEA.
  User: incondensable
  Date: 11/29/20
  Time: 1:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        input[type=search] {
            width: 30%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            background-color: #f8f8f8;
        }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body style="text-align: center;">
<form action="type" method="post">
    Natural Customer <input style="margin-bottom: 10px" type="radio" value="retrieveAllN" name="type"><br>
    Legal Customer <input style="margin-bottom: 10px" type="radio" value="retrieveAllL" name="type"><br>
    <button type="submit" value="submit">Submit</button>
</form>
<form action="cardAccountRequest?" method="post">
    <input type="search" id="customerNumber" name="customerNumber" placeholder="Enter a Customer Number"><br/>
    <button type="submit" value="card" onclick="return isFindAccountEmpty()">Request Card</button>
</form>
<script>
    function isFindAccountEmpty() {
        var customerNumber = document.getElementById("customerNumber").value;
        if (customerNumber === "") {
            alert("Enter a Customer Number");
            return false;
        } else {
            return true;
        }
    }
</script>
</body>
</html>