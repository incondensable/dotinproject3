package com.fanap;

import java.sql.Date;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Test {
    public static void main(String[] args) {
        String dateStr = "1990-09-09";
        LocalDate date = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(dateStr));
        Date newDate = Date.valueOf(date);
        System.out.println(date);
        System.out.println(newDate);
    }
}
