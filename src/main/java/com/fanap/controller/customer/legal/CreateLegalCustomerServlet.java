package com.fanap.controller.customer.legal;

import com.fanap.business.ConvertDateAsStringToDate;
import com.fanap.business.legalcustomer.LegalCustomerUtil;
import com.fanap.dao.customer.LegalCustomersCrudDaoImpl;
import com.fanap.model.customer.LegalCustomers;
import com.fanap.util.LoggingConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

public class CreateLegalCustomerServlet extends HttpServlet {
    private static Logger logger = LogManager.getLogger();

    static {
        LoggingConfig.initializeLogger();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        create(req);
        RequestDispatcher dispatcher = req.getRequestDispatcher("retrieveAllL");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    public void create(HttpServletRequest req) {
        LegalCustomersCrudDaoImpl dao = LegalCustomersCrudDaoImpl.getInstance();

        String name = req.getParameter("companyName");
        String dateStr = req.getParameter("companyDate");
        String code = req.getParameter("economyCode");

        if (name.equals("")) {
            name = null;
        }

        Date date = null;
        if (dateStr.equals("")) {
            dateStr = null;
        } else {
            date = ConvertDateAsStringToDate.convertInputStringDate(dateStr);
        }

        LegalCustomers legalCustomer =
                new LegalCustomers(
                        name,
                        date,
                        Long.parseLong(code)
                );

        legalCustomer.
                setCustomerNumber(
                        LegalCustomerUtil.calculateCustomerNumber(Long.parseLong(code))
                );
        dao.insert(legalCustomer);
    }
}
