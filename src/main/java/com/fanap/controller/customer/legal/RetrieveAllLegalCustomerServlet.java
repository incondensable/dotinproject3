package com.fanap.controller.customer.legal;

import com.fanap.dao.customer.LegalCustomersCrudDaoImpl;
import com.fanap.model.customer.LegalCustomers;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class RetrieveAllLegalCustomerServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getAll(req, resp);
    }

    public void getAll(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LegalCustomersCrudDaoImpl dao = LegalCustomersCrudDaoImpl.getInstance();
        List<LegalCustomers> lListCustomer = dao.getAll();
        req.setAttribute("lListCustomer", lListCustomer);
        RequestDispatcher dispatcher = req.getRequestDispatcher("legalCustomers.jsp");
        dispatcher.forward(req, resp);
    }
}
