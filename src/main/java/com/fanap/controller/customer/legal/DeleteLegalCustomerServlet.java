package com.fanap.controller.customer.legal;

import com.fanap.dao.customer.LegalCustomersCrudDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteLegalCustomerServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("retrieveAllL");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        delete(req);
        doPost(req, resp);
    }

    public void delete(HttpServletRequest req) {
        LegalCustomersCrudDaoImpl dao = LegalCustomersCrudDaoImpl.getInstance();
        long customerNumber = Long.parseLong(req.getParameter("customerNumber"));
        dao.delete(customerNumber);
    }
}
