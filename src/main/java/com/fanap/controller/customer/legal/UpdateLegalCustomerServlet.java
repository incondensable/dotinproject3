package com.fanap.controller.customer.legal;

import com.fanap.business.ConvertDateAsStringToDate;
import com.fanap.business.legalcustomer.LegalCustomerUtil;
import com.fanap.dao.customer.LegalCustomersCrudDaoImpl;
import com.fanap.model.customer.LegalCustomers;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

public class UpdateLegalCustomerServlet extends HttpServlet {
    private LegalCustomersCrudDaoImpl dao = LegalCustomersCrudDaoImpl.getInstance();
    private long customerNumber;
    private LegalCustomers previousLegalCustomer;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        update(req);
        RequestDispatcher dispatcher = req.getRequestDispatcher("findL?customerNumber=" + getCustomerNumber());
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCustomerNumber(Long.parseLong(req.getParameter("customerNumber")));

        setPreviousLegalCustomer(dao.findByCustomerNumber(getCustomerNumber()));
        req.setAttribute("legalCustomer", getPreviousLegalCustomer());

        RequestDispatcher dispatcher = req.getRequestDispatcher("legalCustomerUpdate.jsp");
        dispatcher.forward(req, resp);
    }

    public void update(HttpServletRequest req) {
        String oldName = previousLegalCustomer.getCompanyName();
        Date oldDate = previousLegalCustomer.getDateOfFoundation();
        String oldCode = String.valueOf(previousLegalCustomer.getEconomyCode());

        String name = req.getParameter("companyName");
        String dateStr = req.getParameter("companyDate");
        String code = req.getParameter("economyCode");

        if (name.equals("")) {
            name = oldName;
        }

        Date date;
        if (dateStr.equals("")) {
            date = oldDate;
        } else {
            date = ConvertDateAsStringToDate.convertInputStringDate(dateStr);
        }

        if (code.equals("")) {
            code = oldCode;
        }

        LegalCustomers legalCustomers =
                new LegalCustomers(
                        name,
                        date,
                        Long.parseLong(code)
                );

        legalCustomers.setCompanyId(previousLegalCustomer.getCompanyId());
        setCustomerNumber(
                LegalCustomerUtil.calculateCustomerNumber(Long.parseLong(code))
        );
        legalCustomers.
                setCustomerNumber(
                        getCustomerNumber()
                );

        dao.update(legalCustomers);
    }

    public long getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(long customerNumber) {
        this.customerNumber = customerNumber;
    }

    public LegalCustomers getPreviousLegalCustomer() {
        return previousLegalCustomer;
    }

    public void setPreviousLegalCustomer(LegalCustomers previousLegalCustomer) {
        this.previousLegalCustomer = previousLegalCustomer;
    }
}
