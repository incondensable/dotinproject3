package com.fanap.controller.customer.legal;

import com.fanap.dao.customer.LegalCustomersCrudDaoImpl;
import com.fanap.model.customer.LegalCustomers;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GetLegalCustomerServlet extends HttpServlet {
    private RequestDispatcher dispatcher;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String customerNumber = req.getParameter("customerNumber");
        LegalCustomersCrudDaoImpl dao = LegalCustomersCrudDaoImpl.getInstance();
        LegalCustomers customerById = dao.findByCustomerNumber(Long.parseLong(customerNumber));
        if (dao.evaluate()) {
            boolean check = true;
            req.setAttribute("customerById", customerById);
            req.setAttribute("check", check);
        } else {
            String message = "This User ID is not found";
            boolean check = false;
            req.setAttribute("message", message);
            req.setAttribute("check", check);
        }
        dispatcher = req.getRequestDispatcher("legalCustomerFind.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
