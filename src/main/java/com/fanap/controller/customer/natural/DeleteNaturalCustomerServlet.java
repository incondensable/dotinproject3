package com.fanap.controller.customer.natural;

import com.fanap.dao.cardaccountprofile.CardAccountProfileCrudDaoImpl;
import com.fanap.dao.customer.NaturalCustomersCrudDaoImpl;
import com.fanap.model.customer.NaturalCustomers;
import com.fanap.util.LoggingConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteNaturalCustomerServlet extends HttpServlet {
    private NaturalCustomersCrudDaoImpl dao = NaturalCustomersCrudDaoImpl.getInstance();
    private CardAccountProfileCrudDaoImpl caDao = CardAccountProfileCrudDaoImpl.getInstance();
    private boolean check;

    private Logger logger = LogManager.getLogger();

    static {
        LoggingConfig.initializeLogger();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!isCheck()) {
            String message = "You can't delete this Customer despite being in relation with one or more Card Account(s)";
            req.setAttribute("message", message);
            RequestDispatcher dispatcher = req.getRequestDispatcher("findN");
            dispatcher.forward(req, resp);
        } else {
            RequestDispatcher dispatcher = req.getRequestDispatcher("retrieveAllN");
            dispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        deleteCustomer(req);
        doPost(req, resp);
    }

    public void deleteCustomer(HttpServletRequest req) throws ServletException, IOException {

        long customerNumber = Long.parseLong(req.getParameter("customerNumber"));
        NaturalCustomers naturalCustomer = dao.findByCustomerNumber(customerNumber);
        String action = req.getParameter("action");
        if (action.equals("delete")) {
            if (caDao.findByCardAccountId(naturalCustomer.getId())) {
                dao.delete(customerNumber);
                logger.debug("Natural Customer " + customerNumber + " has been deleted");
                setCheck(true);
            } else {
                setCheck(false);
            }
        }
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
