package com.fanap.controller.customer.natural;

import com.fanap.business.ConvertDateAsStringToDate;
import com.fanap.business.naturalcustomer.NaturalCustomerUtil;
import com.fanap.dao.customer.NaturalCustomersCrudDaoImpl;
import com.fanap.model.customer.NaturalCustomers;
import com.fanap.util.LoggingConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

public class CreateNaturalCustomerServlet extends HttpServlet {
    private NaturalCustomersCrudDaoImpl dao = NaturalCustomersCrudDaoImpl.getInstance();

    private Logger logger = LogManager.getLogger();

    static {
        LoggingConfig.initializeLogger();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher;
        NaturalCustomers naturalCustomer = createAccount(req);

        if (naturalCustomer.getNationalCode() != 0) {
            dao.insert(naturalCustomer);
            if (dao.evaluate()) {
                dispatcher = req.getRequestDispatcher("retrieveAllN");
                dispatcher.forward(req, resp);
            } else {
                String message = "National Code is Duplicated! Choose another one!";
                req.setAttribute("message", message);
                dispatcher = req.getRequestDispatcher("error.jsp");
                dispatcher.forward(req, resp);
            }
        } else {
            String message = "You must Fill the National Code field!";
            req.setAttribute("message", message);
            dispatcher = req.getRequestDispatcher("error.jsp");
            dispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    public NaturalCustomers createAccount(HttpServletRequest req) {
        NaturalCustomers naturalCustomer;

        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String fatherName = req.getParameter("fatherName");
        String dateOfBirthStr = req.getParameter("dateOfBirth");
        String nationalCodeStr = req.getParameter("nationalCode");

        long nationalCode;
        if (nationalCodeStr.isEmpty()) {
            nationalCode = 0L;
        } else {
            nationalCode = Long.parseLong(nationalCodeStr);
        }

        if (firstName.equals("")) {
            firstName = null;
        }

        if (lastName.equals("")) {
            lastName = null;
        }

        if (fatherName.equals("")) {
            fatherName = null;
        }

        Date dateOfBirth = null;
        if (!dateOfBirthStr.equals("")) {
            dateOfBirth = ConvertDateAsStringToDate.convertInputStringDate(dateOfBirthStr);
        }

        naturalCustomer =
                new NaturalCustomers(
                        firstName,
                        lastName,
                        fatherName,
                        dateOfBirth,
                        nationalCode
                );

        naturalCustomer.
                setCustomerNumber(
                        NaturalCustomerUtil.
                                calculateCustomerNumber(nationalCode)
                );

        return naturalCustomer;
    }
}