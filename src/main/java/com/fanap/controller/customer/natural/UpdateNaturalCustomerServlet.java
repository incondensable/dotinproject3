package com.fanap.controller.customer.natural;

import com.fanap.business.ConvertDateAsStringToDate;
import com.fanap.business.naturalcustomer.NaturalCustomerUtil;
import com.fanap.dao.customer.NaturalCustomersCrudDaoImpl;
import com.fanap.model.customer.NaturalCustomers;
import com.fanap.util.LoggingConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

public class UpdateNaturalCustomerServlet extends HttpServlet {
    NaturalCustomersCrudDaoImpl dao = NaturalCustomersCrudDaoImpl.getInstance();
    private String customerNumber;
    private NaturalCustomers previousNaturalCustomer;

    private Logger logger = LogManager.getLogger();
    static {
        LoggingConfig.initializeLogger();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getAndUpdateAccount(req);
        RequestDispatcher dispatcher = req.getRequestDispatcher("findN?customerNumber=" + getCustomerNumber());
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setCustomerNumber(req.getParameter("customerNumber"));
        setNaturalCustomer(dao.findByCustomerNumber(Long.parseLong(customerNumber)));

        req.setAttribute("naturalCustomer", getNaturalCustomer());

        RequestDispatcher dispatcher = req.getRequestDispatcher("naturalCustomerUpdate.jsp");
        dispatcher.forward(req, resp);
    }

    public void getAndUpdateAccount(HttpServletRequest req) {
        String oldFirstName = getNaturalCustomer().getFirstName();
        String oldLastName = getNaturalCustomer().getLastName();
        String oldFatherName = getNaturalCustomer().getFatherName();
        java.sql.Date oldDateOfBirth = getNaturalCustomer().getDateOfBirth();
        String oldNationalCode = String.valueOf(getNaturalCustomer().getNationalCode());
        logger.debug("User input for Updating Account '" + getCustomerNumber() + "'");

        String newFirstName = req.getParameter("firstName");
        if (newFirstName == null || newFirstName.equals("")) {
            newFirstName = oldFirstName;
        }

        String newLastName = req.getParameter("lastName");
        if (newLastName == null || newLastName.equals("")) {
            newLastName = oldLastName;
        }

        String newFatherName = req.getParameter("fatherName");
        if (newFatherName == null || newFatherName.equals("")) {
            newFatherName = oldFatherName;
        }

        String newDateOfBirthStr = req.getParameter("dateOfBirth");
        Date dateOfBirth = null;
        if (newDateOfBirthStr == null || newDateOfBirthStr.equals("")) {
            dateOfBirth = oldDateOfBirth;
        } else {
             dateOfBirth = ConvertDateAsStringToDate.convertInputStringDate(newDateOfBirthStr);
        }

        String newNationalCode = req.getParameter("nationalCode");
        if (newNationalCode == null || newNationalCode.equals("")) {
            newNationalCode = oldNationalCode;
        }

        long nationalCode = Long.parseLong(newNationalCode);

        NaturalCustomers naturalCustomer =
                new NaturalCustomers(
                        newFirstName,
                        newLastName,
                        newFatherName,
                        dateOfBirth,
                        nationalCode
                );

        naturalCustomer.setId(getNaturalCustomer().getId());
        naturalCustomer.
                setCustomerNumber(
                        NaturalCustomerUtil.
                                calculateCustomerNumber(nationalCode)
                );

        dao.update(naturalCustomer);
        logger.debug("Natural Customer Update method has been ended successfully!");
        setCustomerNumber(String.valueOf(naturalCustomer.getCustomerNumber()));
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public NaturalCustomers getNaturalCustomer() {
        return previousNaturalCustomer;
    }

    public void setNaturalCustomer(NaturalCustomers naturalCustomer) {
        this.previousNaturalCustomer = naturalCustomer;
    }
}
