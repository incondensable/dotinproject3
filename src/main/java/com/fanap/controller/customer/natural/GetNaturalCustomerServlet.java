package com.fanap.controller.customer.natural;

import com.fanap.dao.customer.NaturalCustomersCrudDaoImpl;
import com.fanap.model.customer.NaturalCustomers;
import com.fanap.util.LoggingConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GetNaturalCustomerServlet extends HttpServlet {
    private NaturalCustomersCrudDaoImpl dao = NaturalCustomersCrudDaoImpl.getInstance();

    private Logger logger = LogManager.getLogger();
    static {
        LoggingConfig.initializeLogger();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long customerNumber = Long.parseLong(req.getParameter("customerNumber"));
        logger.debug("User entered " + customerNumber + " for finding a Natural Customer");
        NaturalCustomers customerByCustomerNumber = dao.findByCustomerNumber(customerNumber);
        logger.debug("Natural Customer get method has been ended successfully");
        if (customerByCustomerNumber != null) {
            req.setAttribute("customerByCustomerNumber", customerByCustomerNumber);
        } else {
            String message = "User with this Customer Number can't be not found";
            req.setAttribute("message", message);
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher("naturalCustomerFind.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
