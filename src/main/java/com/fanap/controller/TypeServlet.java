package com.fanap.controller;

import com.fanap.util.LoggingConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TypeServlet extends HttpServlet {
    private Logger logger = LogManager.getLogger();
    static {
        LoggingConfig.initializeLogger();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getParameter("type");

        RequestDispatcher dispatcher;
        if (type != null) {
            if (type.equals("retrieveAllN")) {
                logger.debug("User selected the: " + type);
                dispatcher = req.getRequestDispatcher("retrieveAllN");
                dispatcher.forward(req, resp);
            } else if (type.equals("retrieveAllL")) {
                logger.debug("User selected the: " + type);
                dispatcher = req.getRequestDispatcher("retrieveAllL");
                dispatcher.forward(req, resp);
            }
        } else {
            logger.debug("User selected NONE of The Customer Types");
            dispatcher = req.getRequestDispatcher("customerType.jsp");
            dispatcher.forward(req, resp);
        }
    }
}