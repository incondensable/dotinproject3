package com.fanap.controller.card_account.card_account_group;

import com.fanap.dao.cardaccount.CardAccountGroupCrudDaoImpl;
import com.fanap.dao.cardaccount.CreditProfileCrudDaoImpl;
import com.fanap.model.card.CardAccountGroup;
import com.fanap.model.card.CreditProfile;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CardAccountGroupUpdateServlet extends HttpServlet {
    private CardAccountGroupCrudDaoImpl cagDao = CardAccountGroupCrudDaoImpl.getInstance();
    private CreditProfileCrudDaoImpl cpDao = CreditProfileCrudDaoImpl.getInstance();
    private CardAccountGroup cardAccountGroup;
    private CreditProfile creditProfile;
    private RequestDispatcher dispatcher;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        update(req);
        dispatcher = req.getRequestDispatcher("cardAccountGroupMng");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long customerNumber = Long.parseLong(req.getParameter("customerNumber"));
        int cardAccountGroupId = Integer.parseInt(req.getParameter("cardAccountGroupId"));
        String creditProfileId = req.getParameter("creditProfileId");

        setCardAccountGroup(cagDao.findById(cardAccountGroupId));
        if (creditProfileId.equals("null")) {
            req.setAttribute("creditProfile", null);
        } else {
            setCreditProfile(cpDao.findById(Integer.parseInt(creditProfileId)));
            req.setAttribute("creditProfile", getCreditProfile());
        }
        List<CreditProfile> creditProfileList = cpDao.getAll();

        req.setAttribute("cardAccountGroup", getCardAccountGroup());
        req.setAttribute("customerNumber", customerNumber);
        req.setAttribute("creditProfileList", creditProfileList);

        dispatcher = req.getRequestDispatcher("card/cardAccountGroupUpdate.jsp");
        dispatcher.forward(req, resp);
    }

    public void update(HttpServletRequest req) {
        String cardAccountGroupDescription = req.getParameter("cardAccountGroupDescription");
        String creditProfileDesc = req.getParameter("creditProfileDesc");

        CreditProfile creditProfile = cpDao.findByDescription(creditProfileDesc);

        String oldCardAccountGroupDescription = getCardAccountGroup().getDescription();

        if (cardAccountGroupDescription.equals("")) {
            cardAccountGroupDescription = oldCardAccountGroupDescription;
        }

        CardAccountGroup cardAccountGroup = new CardAccountGroup();
        cardAccountGroup.setId(getCardAccountGroup().getId());
        cardAccountGroup.setDescription(cardAccountGroupDescription);
        cardAccountGroup.setCreditProfile(creditProfile);
        cagDao.update(cardAccountGroup);
    }

    public CardAccountGroup getCardAccountGroup() {
        return cardAccountGroup;
    }

    public void setCardAccountGroup(CardAccountGroup cardAccountGroup) {
        this.cardAccountGroup = cardAccountGroup;
    }

    public CreditProfile getCreditProfile() {
        return creditProfile;
    }

    public void setCreditProfile(CreditProfile creditProfile) {
        this.creditProfile = creditProfile;
    }
}