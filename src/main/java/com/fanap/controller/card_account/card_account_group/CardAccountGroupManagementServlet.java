package com.fanap.controller.card_account.card_account_group;

import com.fanap.dao.cardaccount.CardAccountGroupCrudDaoImpl;
import com.fanap.model.card.CardAccountGroup;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CardAccountGroupManagementServlet extends HttpServlet {
    private CardAccountGroupCrudDaoImpl dao = CardAccountGroupCrudDaoImpl.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long customerNumber = Long.parseLong(req.getParameter("customerNumber"));
        List<CardAccountGroup> cardAccountGroupList = dao.getAll();
        req.setAttribute("cardAccountGroupList", cardAccountGroupList);
        req.setAttribute("customerNumber", customerNumber);
        RequestDispatcher dispatcher = req.getRequestDispatcher("card/cardAccountGroupManagement.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
