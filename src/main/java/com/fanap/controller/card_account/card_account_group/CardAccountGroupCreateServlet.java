package com.fanap.controller.card_account.card_account_group;

import com.fanap.dao.cardaccount.CardAccountGroupCrudDaoImpl;
import com.fanap.dao.cardaccount.CreditProfileCrudDaoImpl;
import com.fanap.model.card.CardAccountGroup;
import com.fanap.model.card.CreditProfile;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CardAccountGroupCreateServlet extends HttpServlet {
    private final CreditProfileCrudDaoImpl CP_DAO = CreditProfileCrudDaoImpl.getInstance();
    private final CardAccountGroupCrudDaoImpl CAG_DAO = CardAccountGroupCrudDaoImpl.getInstance();
    private List<CreditProfile> creditProfileList = CP_DAO.getAll();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        long customerNumber = Long.parseLong(req.getParameter("customerNumber"));
        RequestDispatcher dispatcher;
        if ("post".equals(action)) {
            req.setAttribute("creditProfileList", creditProfileList);
            req.setAttribute("customerNumber", customerNumber);
            dispatcher = req.getRequestDispatcher("card/cardAccountGroupRegister.jsp");
        } else {
            createAccount(req);
            dispatcher = req.getRequestDispatcher("cardAccountGroupMng");
        }
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    public void createAccount(HttpServletRequest req) {
        CardAccountGroup cardAccountGroup;
        String description = req.getParameter("description");
        String creditProfileId = req.getParameter("creditProfileId");

        CreditProfile creditProfile = CP_DAO.findById(Integer.parseInt(creditProfileId));

        cardAccountGroup = new CardAccountGroup();
        cardAccountGroup.setDescription(description);
        cardAccountGroup.setCreditProfile(creditProfile);

        CardAccountGroup oldCardAccountGroup = CAG_DAO.findByDescription(description);
        if (oldCardAccountGroup == null) CAG_DAO.insert(cardAccountGroup);
    }
}