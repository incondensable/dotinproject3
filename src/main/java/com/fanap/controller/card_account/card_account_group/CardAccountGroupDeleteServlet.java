package com.fanap.controller.card_account.card_account_group;

import com.fanap.dao.cardaccountprofile.CardAccountProfileCrudDaoImpl;
import com.fanap.dao.cardaccount.CardAccountGroupCrudDaoImpl;
import com.fanap.dao.customer.NaturalCustomersCrudDaoImpl;
import com.fanap.model.card.CardAccountGroup;
import com.fanap.model.customer.NaturalCustomers;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CardAccountGroupDeleteServlet extends HttpServlet {
    private CardAccountGroupCrudDaoImpl dao = CardAccountGroupCrudDaoImpl.getInstance();
    private CardAccountProfileCrudDaoImpl caDao = CardAccountProfileCrudDaoImpl.getInstance();
    private NaturalCustomersCrudDaoImpl ncDao = NaturalCustomersCrudDaoImpl.getInstance();
    private boolean check;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        delete(req);
        if (!isCheck()) {
            String message = "This Card Account Group has a relation to a persisted Card Account! So You can't Delete this Record";
            req.setAttribute("message", message);
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher("cardAccountGroupMng");
        dispatcher.forward(req, resp);
    }

    public void delete(HttpServletRequest req) {
        long customerNumber = Long.parseLong(req.getParameter("customerNumber"));
        int cardAccountGroupId = Integer.parseInt(req.getParameter("cardAccountGroupId"));
        req.setAttribute("customerNumber", customerNumber);
        CardAccountGroup cardAccountGroup = dao.findById(cardAccountGroupId);
        NaturalCustomers naturalCustomer = ncDao.findByCustomerNumber(customerNumber);
        if (caDao.findByCardAccountGroupIdAndCustomerId(cardAccountGroup.getId(), naturalCustomer.getId()) == null) {
            dao.delete(cardAccountGroup);
            setCheck(true);
        } else {
            setCheck(false);
        }
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
