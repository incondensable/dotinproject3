package com.fanap.controller.card_account;

import com.fanap.dao.cardaccount.CardAccountGroupCrudDaoImpl;
import com.fanap.dao.customer.NaturalCustomersCrudDaoImpl;
import com.fanap.model.card.CardAccountGroup;
import com.fanap.model.customer.NaturalCustomers;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CardAccountRequestServlet extends HttpServlet {
    private NaturalCustomersCrudDaoImpl daoN = NaturalCustomersCrudDaoImpl.getInstance();
    private CardAccountGroupCrudDaoImpl cagDao = CardAccountGroupCrudDaoImpl.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //
        String customerNumberStr = req.getParameter("customerNumber");
        long customerNumber;
        if (!customerNumberStr.equals("")) {
            customerNumber = Long.parseLong(customerNumberStr);
        } else {
            customerNumber = 0L;
        }
        List<CardAccountGroup> cardAccountGroupList = cagDao.getAll();
        NaturalCustomers customerByCustomerNumber = daoN.findByCustomerNumber(customerNumber);
        if (customerByCustomerNumber != null) {
            req.setAttribute("customerByCustomerNumber", customerByCustomerNumber);
            req.setAttribute("cardAccountGroupList", cardAccountGroupList);
        } else {
            String message = "User with this Customer Number can't be not found";
            req.setAttribute("message", message);
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher("card/creditAccountMng.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
