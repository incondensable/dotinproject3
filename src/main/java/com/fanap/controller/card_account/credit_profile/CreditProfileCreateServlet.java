package com.fanap.controller.card_account.credit_profile;

import com.fanap.dao.cardaccount.CreditProfileCrudDaoImpl;
import com.fanap.model.card.CreditProfile;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CreditProfileCreateServlet extends HttpServlet {
    private CreditProfileCrudDaoImpl dao = CreditProfileCrudDaoImpl.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        long customerNumber = Long.parseLong(req.getParameter("customerNumber"));
        RequestDispatcher dispatcher;
        if ("post".equals(action)) {
            req.setAttribute("customerNumber", customerNumber);
            dispatcher = req.getRequestDispatcher("card/creditProfileRegister.jsp");
        } else {
            createAccount(req);
            dispatcher = req.getRequestDispatcher("creditProfileMng");
        }
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    public void createAccount(HttpServletRequest req) {
        CreditProfile creditProfile;

        String description = req.getParameter("description");
        String minBound = req.getParameter("minBound");
        String maxBound = req.getParameter("maxBound");

        creditProfile = new CreditProfile();
        creditProfile.setDescription(description);
        creditProfile.setMinCreditBound(Float.parseFloat(minBound));
        creditProfile.setMaxCreditBound(Float.parseFloat(maxBound));

        CreditProfile oldCreditProfile = dao.findByDescription(description);
        if (oldCreditProfile == null) dao.insert(creditProfile);
    }
}
