package com.fanap.controller.card_account.credit_profile;

import com.fanap.dao.cardaccount.CardAccountGroupCrudDaoImpl;
import com.fanap.dao.cardaccount.CreditProfileCrudDaoImpl;
import com.fanap.model.card.CreditProfile;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CreditProfileDeleteServlet extends HttpServlet {
    private CreditProfileCrudDaoImpl dao = CreditProfileCrudDaoImpl.getInstance();
    private CardAccountGroupCrudDaoImpl cagDao = CardAccountGroupCrudDaoImpl.getInstance();
    private boolean check;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        delete(req);
        if (!isCheck()) {
            String message = "You can't delete this Credit Profile because it's got a relation to one or more Card Account Group(s)";
            req.setAttribute("message", message);
        }
        RequestDispatcher dispatcher = req.getRequestDispatcher("creditProfileMng");
        dispatcher.forward(req, resp);
    }

    public void delete(HttpServletRequest req) {
        long customerNumber = Long.parseLong(req.getParameter("customerNumber"));
        int creditProfileId = Integer.parseInt(req.getParameter("creditProfileId"));
        req.setAttribute("customerNumber", customerNumber);
        CreditProfile creditProfile = dao.findById(creditProfileId);
        if (cagDao.findByCpId(creditProfileId)) {
            dao.delete(creditProfile);
            setCheck(true);
        } else {
            setCheck(false);
        }
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}