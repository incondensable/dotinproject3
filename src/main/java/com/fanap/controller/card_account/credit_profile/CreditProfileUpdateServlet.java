package com.fanap.controller.card_account.credit_profile;

import com.fanap.dao.cardaccount.CreditProfileCrudDaoImpl;
import com.fanap.model.card.CreditProfile;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CreditProfileUpdateServlet extends HttpServlet {
    private CreditProfileCrudDaoImpl dao = CreditProfileCrudDaoImpl.getInstance();
    private CreditProfile creditProfile;
    private RequestDispatcher dispatcher;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        update(req);
        dispatcher = req.getRequestDispatcher("creditProfileMng");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long customerNumber = Long.parseLong(req.getParameter("customerNumber"));
        int creditProfileId = Integer.parseInt(req.getParameter("creditProfileId"));
        setCreditProfile(dao.findById(creditProfileId));

        req.setAttribute("creditProfile", getCreditProfile());
        req.setAttribute("customerNumber", customerNumber);

        dispatcher = req.getRequestDispatcher("card/creditProfileUpdate.jsp");
        dispatcher.forward(req, resp);
    }

    public void update(HttpServletRequest req) {
        String creditProfileDesc = req.getParameter("creditProfileDesc");
        String creditProfileMinimumBoundStr = req.getParameter("creditProfileMinimumBound");
        String creditProfileMaximumBoundStr = req.getParameter("creditProfileMaximumBound");

        String oldCreditProfileDesc = getCreditProfile().getDescription();
        float oldCreditProfileMinimumBound = getCreditProfile().getMinCreditBound();
        float oldCreditProfileMaximumBound = getCreditProfile().getMaxCreditBound();

        if (creditProfileDesc.equals("")) {
            creditProfileDesc = oldCreditProfileDesc;
        }

        float creditProfileMinimumBound;
        if (creditProfileMinimumBoundStr.equals("")) {
            creditProfileMinimumBound = oldCreditProfileMinimumBound;
        } else {
             creditProfileMinimumBound = Float.parseFloat(creditProfileMinimumBoundStr);
        }

        float creditProfileMaximumBound;
        if (creditProfileMaximumBoundStr.equals("")) {
            creditProfileMaximumBound = oldCreditProfileMaximumBound;
        } else {
            creditProfileMaximumBound = Float.parseFloat(creditProfileMaximumBoundStr);
        }

        CreditProfile creditProfile = new CreditProfile();
        creditProfile.setId(getCreditProfile().getId());
        creditProfile.setDescription(creditProfileDesc);
        creditProfile.setMinCreditBound(creditProfileMinimumBound);
        creditProfile.setMaxCreditBound(creditProfileMaximumBound);

        dao.update(creditProfile);
    }

    public CreditProfile getCreditProfile() {
        return creditProfile;
    }

    public void setCreditProfile(CreditProfile creditProfile) {
        this.creditProfile = creditProfile;
    }
}