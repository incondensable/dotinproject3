package com.fanap.controller.card_account.credit_profile;

import com.fanap.dao.cardaccount.CreditProfileCrudDaoImpl;
import com.fanap.model.card.CreditProfile;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CreditProfileManagementServlet extends HttpServlet {
    private CreditProfileCrudDaoImpl dao = CreditProfileCrudDaoImpl.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long customerNumber = Long.parseLong(req.getParameter("customerNumber"));
        List<CreditProfile> creditProfileList = dao.getAll();
        req.setAttribute("customerNumber", customerNumber);
        req.setAttribute("creditProfileList", creditProfileList);
        RequestDispatcher dispatcher = req.getRequestDispatcher("card/creditProfileManagement.jsp");
        dispatcher.forward(req, resp);
        super.doPost(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
