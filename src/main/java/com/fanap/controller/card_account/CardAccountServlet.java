package com.fanap.controller.card_account;

import com.fanap.dao.cardaccountprofile.CardAccountProfileCrudDaoImpl;
import com.fanap.dao.cardaccount.CardAccountGroupCrudDaoImpl;
import com.fanap.dao.customer.NaturalCustomersCrudDaoImpl;
import com.fanap.business.cardaccountprofile.CardAccountProfileUtil;
import com.fanap.model.card.CardAccount;
import com.fanap.model.card.CardAccountGroup;
import com.fanap.model.customer.NaturalCustomers;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CardAccountServlet extends HttpServlet {
    private CardAccountGroupCrudDaoImpl cagDao = CardAccountGroupCrudDaoImpl.getInstance();
    private NaturalCustomersCrudDaoImpl customerDao = NaturalCustomersCrudDaoImpl.getInstance();
    private CardAccountProfileCrudDaoImpl caDao = CardAccountProfileCrudDaoImpl.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long customerNumber = Long.parseLong(req.getParameter("customerNumber"));
        int cagId = Integer.parseInt(req.getParameter("cagId"));

        CardAccountGroup cardAccountGroup = cagDao.findById(cagId);
        NaturalCustomers naturalCustomer = customerDao.findByCustomerNumber(customerNumber);

        CardAccount cardAccount = new CardAccount();
        cardAccount.setNaturalCustomers(naturalCustomer);
        cardAccount.setCardAccountGroup(cardAccountGroup);
        CardAccount oldCardAccount = caDao.findByCardAccountGroupIdAndCustomerId(cagId, naturalCustomer.getId());
        boolean check = true;

        if (oldCardAccount == null) {
            cardAccount.setFileNumber(
                    CardAccountProfileUtil.
                            calculateFileNumber(
                                    naturalCustomer.getId(),
                                    cardAccountGroup.getId())
            );

            caDao.insert(cardAccount);
        } else {
            cardAccount = oldCardAccount;
            check = false;
        }

        req.setAttribute("cardAccount", cardAccount);
        req.setAttribute("customerNumber", customerNumber);

        RequestDispatcher dispatcher;
        if (check) {
            dispatcher = req.getRequestDispatcher("card/cardAccount.jsp");
        } else {
            dispatcher = req.getRequestDispatcher("card/cardAccountError.jsp");
        }
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
