package com.fanap.business;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ConvertDateAsStringToDate {
    public static Date convertInputStringDate(String dateOfBirthStr) {
        LocalDate date = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(dateOfBirthStr));
        return Date.valueOf(date);
    }
}
