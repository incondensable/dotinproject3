package com.fanap.business.cardaccountprofile;

import com.fanap.util.LoggingConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CardAccountProfileUtil {
    private static Logger logger = LogManager.getLogger();

    static {
        LoggingConfig.initializeLogger();
    }
    public static Integer calculateFileNumber(int naturalCustomersId, int cardAccountGroupId) {
        logger.debug("Card Account File Number is getting created!");
        String long1 = String.valueOf(1000);
        String long2 = String.valueOf(naturalCustomersId);
        String long3 = String.valueOf(cardAccountGroupId);
        return Integer.parseInt(long1.concat(long2.concat(long3)));
    }
}