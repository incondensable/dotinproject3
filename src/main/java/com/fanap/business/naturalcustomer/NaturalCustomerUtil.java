package com.fanap.business.naturalcustomer;

import com.fanap.dao.customer.NaturalCustomersCrudDaoImpl;
import com.fanap.util.LoggingConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NaturalCustomerUtil {
    private static NaturalCustomersCrudDaoImpl dao = NaturalCustomersCrudDaoImpl.getInstance();
    private static Logger logger = LogManager.getLogger();

    static {
        LoggingConfig.initializeLogger();
    }

    public static Long calculateCustomerNumber(long naturalCustomerNationalCode) {
        logger.debug("Natural Customer's Customer Number is getting created!");
        String long1 = String.valueOf(1000);
        String long2 = String.valueOf(naturalCustomerNationalCode);
        return Long.parseLong(long1.concat(long2));
    }

    // for when Natural Customer is getting created
//    public static String checkNaturalCustomer(NaturalCustomers naturalCustomer) {
//        dao.insert(naturalCustomer);
//    }
}
