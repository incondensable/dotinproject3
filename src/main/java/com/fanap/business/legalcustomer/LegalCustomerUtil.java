package com.fanap.business.legalcustomer;

import com.fanap.util.LoggingConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LegalCustomerUtil {
    private static Logger logger = LogManager.getLogger();

    static {
        LoggingConfig.initializeLogger();
    }

    public static Long calculateCustomerNumber(long companyEconomyCode) {
        logger.debug("Legal Customer's Customer Number is getting created");
        String long1 = String.valueOf(1000);
        String long2 = String.valueOf(companyEconomyCode);
        return Long.parseLong(long1.concat(long2));
    }
}
