package com.fanap.util;

import com.fanap.model.card.CardAccount;
import com.fanap.model.card.CardAccountGroup;
import com.fanap.model.card.CreditProfile;
import com.fanap.model.customer.LegalCustomers;
import com.fanap.model.customer.NaturalCustomers;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        try {
            Configuration configuration = new Configuration().configure();

            configuration.addAnnotatedClass(NaturalCustomers.class);
            configuration.addAnnotatedClass(LegalCustomers.class);

            configuration.addAnnotatedClass(CardAccount.class);
            configuration.addAnnotatedClass(CardAccountGroup.class);
            configuration.addAnnotatedClass(CreditProfile.class);

            sessionFactory = configuration.buildSessionFactory();
            return sessionFactory;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return sessionFactory;
    }
}