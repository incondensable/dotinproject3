package com.fanap.util;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.*;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;

public class LoggingConfig {
    public static void initializeLogger() {
        String filename = "/Users/incondensable/IdeaProjects/fanapProject3/logs/out.log";
        String pattern = "%d %p %c [%t] %m%n";

        ConfigurationBuilder<BuiltConfiguration> builder =
                ConfigurationBuilderFactory.newConfigurationBuilder();

        builder.setStatusLevel(Level.DEBUG);
        builder.setConfigurationName("DefaultFileLogger");

        // specifying the pattern layout
        LayoutComponentBuilder layoutBuilder = builder.newLayout("PatternLayout")
                .addAttribute("pattern", pattern);

        // create a file appender
        AppenderComponentBuilder appenderBuilder = builder.newAppender("LogToFile", "File")
                .addAttribute("filename", filename)
                .add(layoutBuilder);

        RootLoggerComponentBuilder rootLogger = builder.newRootLogger(Level.DEBUG)
                .addAttribute("metrics", false);

        builder.add(appenderBuilder);
        rootLogger.add(builder.newAppenderRef("LogToFile"));
        rootLogger.addAttribute("Multi-Release", true);
        builder.add(rootLogger);
        Configurator.reconfigure(builder.build());
    }

}
