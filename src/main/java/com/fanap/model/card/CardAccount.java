package com.fanap.model.card;

import com.fanap.model.customer.NaturalCustomers;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "CardAccount")
public class CardAccount {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "creationDate")
    private Timestamp creationDate = new Timestamp(new Date().getTime());

    @JoinColumn(name = "customerId")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private NaturalCustomers naturalCustomers;

    @JoinColumn(name = "cardAccountGroupId")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private CardAccountGroup cardAccountGroup;

    @Column(name = "fileNumber")
    private int fileNumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public NaturalCustomers getNaturalCustomers() {
        return naturalCustomers;
    }

    public void setNaturalCustomers(NaturalCustomers naturalCustomers) {
        this.naturalCustomers = naturalCustomers;
    }

    public CardAccountGroup getCardAccountGroup() {
        return cardAccountGroup;
    }

    public void setCardAccountGroup(CardAccountGroup cardAccountGroup) {
        this.cardAccountGroup = cardAccountGroup;
    }

    public int getFileNumber() {
        return fileNumber;
    }

    public void setFileNumber(int fileNumber) {
        this.fileNumber = fileNumber;
    }
}