package com.fanap.model.card;

import javax.persistence.*;

@Entity
@Table(name = "CardAccountGroup")
public class CardAccountGroup {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "description")
    private String description;

    @JoinColumn(name = "creditProfileId")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    private CreditProfile creditProfile;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CreditProfile getCreditProfile() {
        return creditProfile;
    }

    public void setCreditProfile(CreditProfile creditProfile) {
        this.creditProfile = creditProfile;
    }
}
