package com.fanap.model.card;

import javax.persistence.*;

@Entity
@Table(name = "CreditProfile")
public class CreditProfile {
    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "description")
    private String description;

    @Column(name = "minCreditBound")
    private float minCreditBound;

    @Column(name = "maxCreditBound")
    private float maxCreditBound;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getMinCreditBound() {
        return minCreditBound;
    }

    public void setMinCreditBound(float minCreditBound) {
        this.minCreditBound = minCreditBound;
    }

    public float getMaxCreditBound() {
        return maxCreditBound;
    }

    public void setMaxCreditBound(float maxCreditBound) {
        this.maxCreditBound = maxCreditBound;
    }
}
