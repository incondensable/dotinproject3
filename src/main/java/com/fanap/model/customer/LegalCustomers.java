package com.fanap.model.customer;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "LegalCustomers")
public class LegalCustomers {
    @Id
    @Column(name = "companyId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int companyId;

    @Column(name = "companyName")
    private String companyName;

    @Column(name = "dateOfFoundation")
    private Date dateOfFoundation;

    @Column(name = "economyCode")
    private long economyCode;

    @Column(name = "customerNumber")
    private long customerNumber;

    public LegalCustomers() {
    }

    public LegalCustomers(String companyName, Date dateOfFoundation, long economyCode) {
        this.companyName = companyName;
        this.dateOfFoundation = dateOfFoundation;
        this.economyCode = economyCode;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Date getDateOfFoundation() {
        return dateOfFoundation;
    }

    public void setDateOfFoundation(Date dateOfFoundation) {
        this.dateOfFoundation = dateOfFoundation;
    }

    public long getEconomyCode() {
        return economyCode;
    }

    public void setEconomyCode(long economyCode) {
        this.economyCode = economyCode;
    }

    public long getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(long customerNumber) {
        this.customerNumber = customerNumber;
    }
}
