package com.fanap.model.customer;

import com.sun.istack.internal.NotNull;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "NaturalCustomers")
public class NaturalCustomers {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "fatherName")
    private String fatherName;

    @Column(name = "dateOfBirth")
    private Date dateOfBirth;

    @NotNull
    @Column(name = "nationalCode")
    private long nationalCode;

    @Column(name = "customerNumber")
    private long customerNumber;

    public NaturalCustomers() {
    }

    public NaturalCustomers(String firstName, String lastName, String fatherName, java.sql.Date dateOfBirth, long nationalCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.dateOfBirth = dateOfBirth;
        this.nationalCode = nationalCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(java.sql.Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public long getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(long nationalCode) {
        this.nationalCode = nationalCode;
    }

    public long getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(long customerNumber) {
        this.customerNumber = customerNumber;
    }

    public void setCustomerNumber() {
        String long1 = String.valueOf(1000);
        String long2 = String.valueOf(getNationalCode());
        this.customerNumber = Long.parseLong(long1.concat(long2));
    }
}
