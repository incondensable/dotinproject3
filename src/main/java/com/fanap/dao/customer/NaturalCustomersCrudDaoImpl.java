package com.fanap.dao.customer;

import com.fanap.model.customer.NaturalCustomers;
import com.fanap.util.HibernateUtil;
import com.fanap.util.LoggingConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
public class NaturalCustomersCrudDaoImpl implements CustomerCrudDao<NaturalCustomers> {
    private Logger logger = LogManager.getLogger();
    private static NaturalCustomersCrudDaoImpl instance = null;
    private boolean valid = true;

    private NaturalCustomersCrudDaoImpl() {

    }

    static {
        LoggingConfig.initializeLogger();
    }

    public static NaturalCustomersCrudDaoImpl getInstance() {
        if (instance == null) {
            instance = new NaturalCustomersCrudDaoImpl();
            return instance;
        }
        return instance;
    }

    @Override
    public List<NaturalCustomers> getAll() {
        List<NaturalCustomers> naturalCustomers = new ArrayList<>();

        logger.debug("Natural Customers Get All method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            Query query = session.createQuery("FROM NaturalCustomers");
            List<NaturalCustomers> list = query.list();
            naturalCustomers.addAll(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return naturalCustomers;
    }

    @Override
    public NaturalCustomers findByCustomerNumber(long customerNumber) {
        NaturalCustomers naturalCustomer;
        logger.debug("Natural Customers Get method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            naturalCustomer = (NaturalCustomers) session.createQuery("FROM NaturalCustomers WHERE customerNumber=" + customerNumber).getSingleResult();
            session.getTransaction().commit();
            return naturalCustomer;
        } catch (NoResultException nre) {
            return null;
        }
    }

    @Override
    public void delete(long customerNumber) {
        logger.debug("Natural Customers Delete method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            NaturalCustomers naturalCustomer =
                    (NaturalCustomers) session.createQuery(
                            "FROM NaturalCustomers WHERE customerNumber=" + customerNumber)
                            .getSingleResult();
            session.delete(naturalCustomer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(NaturalCustomers naturalCustomer) {
        logger.debug("Natural Customers Update method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.merge(naturalCustomer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void insert(NaturalCustomers naturalCustomer) {
        logger.debug("Natural Customers Insert method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(naturalCustomer);
            setValid(true);
        } catch (Exception e) {
            setValid(false);
        }
    }

    @Override
    public boolean evaluate() {
        return isValid();
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
