package com.fanap.dao.customer;

import com.fanap.model.customer.LegalCustomers;
import com.fanap.util.HibernateUtil;
import com.fanap.util.LoggingConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class LegalCustomersCrudDaoImpl implements CustomerCrudDao<LegalCustomers> {
    private static LegalCustomersCrudDaoImpl instance = null;
    private boolean valid = true;
    private Logger logger = LogManager.getLogger();

    static {
        LoggingConfig.initializeLogger();
    }

    private LegalCustomersCrudDaoImpl() {
    }

    public static LegalCustomersCrudDaoImpl getInstance() {
        if (instance == null) {
            instance = new LegalCustomersCrudDaoImpl();
            return instance;
        }
        return instance;
    }

    @Override
    public List<LegalCustomers> getAll() {
        List<LegalCustomers> legalCustomers = new ArrayList<>();
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.beginTransaction();
            Query query = session.createQuery("FROM LegalCustomers");
            List<LegalCustomers> list = query.list();
            legalCustomers.addAll(list);
        }
        return legalCustomers;
    }

    @Override
    public LegalCustomers findByCustomerNumber(long customerNumber) {
        LegalCustomers legalCustomer;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            legalCustomer = (LegalCustomers) session.createQuery("FROM LegalCustomers WHERE customerNumber='" + customerNumber + "'").getSingleResult();
            session.getTransaction().commit();
            if (legalCustomer == null) {
                return null;
            } else {
                return legalCustomer;
            }
        }
    }

    @Override
    public void delete(long customerNumber) {
        LegalCustomers legalCustomer;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            legalCustomer = (LegalCustomers) session.createQuery
                    ("FROM LegalCustomers WHERE customerNumber='" + customerNumber + "'")
                    .getSingleResult();
            session.delete(legalCustomer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(LegalCustomers legalCustomer) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.merge(legalCustomer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void insert(LegalCustomers legalCustomer) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(legalCustomer);
        }
    }

    @Override
    public boolean evaluate() {
        return isValid();
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}