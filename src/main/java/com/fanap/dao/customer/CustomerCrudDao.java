package com.fanap.dao.customer;

import java.util.List;

public interface CustomerCrudDao<T> {
    List<T> getAll();

    T findByCustomerNumber(long customerNumber);

    void delete(long customerNumber);

    void update(T t);

    void insert(T t);

    boolean evaluate();
}
