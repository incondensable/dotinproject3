package com.fanap.dao.cardaccount;

import com.fanap.model.card.CardAccountGroup;
import com.fanap.util.HibernateUtil;
import com.fanap.util.LoggingConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

public class CardAccountGroupCrudDaoImpl implements CardAccountCrudDao<CardAccountGroup> {
    private static CardAccountGroupCrudDaoImpl instance = null;
    private final Logger logger = LogManager.getLogger();
    private CardAccountGroupCrudDaoImpl() {
    }
    public static CardAccountGroupCrudDaoImpl getInstance() {
        if (instance == null) {
            instance = new CardAccountGroupCrudDaoImpl();
            return instance;
        }
        return instance;
    }
    static {
        LoggingConfig.initializeLogger();
    }

    @Override
    public List<CardAccountGroup> getAll() {
        List<CardAccountGroup> cardAccountGroupList = new ArrayList<>();
        logger.debug("Card Account Group Get All method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            Query query = session.createQuery("From CardAccountGroup");
            List<CardAccountGroup> list = query.list();
            cardAccountGroupList.addAll(list);
            session.getTransaction().commit();
            return cardAccountGroupList;
        }
    }

    @Override
    public CardAccountGroup findById(int id) {
        CardAccountGroup cardAccountGroup;
        logger.debug("Card Account Group Find By Id method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            cardAccountGroup = session.get(CardAccountGroup.class, id);
            session.getTransaction().commit();
            return cardAccountGroup;
        } catch (NoResultException nre) {
            return null;
        }
    }

    @Override
    public CardAccountGroup findByDescription(String description) {
        CardAccountGroup cardAccountGroup;
        logger.debug("Card Account Group Find By Name method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            Query query = session.createQuery("FROM CardAccountGroup WHERE description = '" + description + "'");
            cardAccountGroup = (CardAccountGroup) query.uniqueResult();
            session.getTransaction().commit();
            return cardAccountGroup;
        } catch (NoResultException nre) {
            return null;
        }
    }

    public CardAccountGroup findByNameAndId(String name, int creditProfileId) {
        CardAccountGroup cardAccountGroup;
        logger.debug("Card Account Group Find By Name And Id method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            Query query =
                    session.createQuery
                            ("FROM CardAccountGroup WHERE description = '" + name + "'" + "AND creditProfile = '" + creditProfileId + "'");
            cardAccountGroup = (CardAccountGroup) query.uniqueResult();
            session.getTransaction().commit();
            return cardAccountGroup;
        } catch (NoResultException nre) {
            return null;
        }
    }

    public boolean findByCpId(int cpId) {
        List<CardAccountGroup> cardAccountGroups;
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            Query query =
                    session.createQuery(
                            "FROM CardAccountGroup WHERE creditProfile.id='" + cpId + "'"
                    );
            cardAccountGroups = (List<CardAccountGroup>) query.getResultList();
            session.getTransaction().commit();
            return cardAccountGroups.size() == 0;
        }
    }

    @Override
    public void delete(CardAccountGroup cardAccountGroup) {
        logger.debug("Card Account Group Delete method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            session.remove(cardAccountGroup);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(CardAccountGroup cardAccountGroup) {
        logger.debug("Card Account Group Update method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            session.merge(cardAccountGroup);
            session.getTransaction().commit();
        }
    }

    @Override
    public void insert(CardAccountGroup cardAccountGroup) {
        logger.debug("Card Account Group Insert method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            session.save(cardAccountGroup);
            session.getTransaction().commit();
        }
    }
}