package com.fanap.dao.cardaccount;

import java.util.List;

public interface CardAccountCrudDao<T> {
        List<T> getAll();

        T findById(int id);

        T findByDescription(String description);

        void delete(T t);

        void update(T t);

        void insert(T t);
}