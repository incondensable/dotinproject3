package com.fanap.dao.cardaccount;

import com.fanap.model.card.CreditProfile;
import com.fanap.util.HibernateUtil;
import com.fanap.util.LoggingConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

public class CreditProfileCrudDaoImpl implements CardAccountCrudDao<CreditProfile> {
    private static CreditProfileCrudDaoImpl instance = null;
    private final Logger logger = LogManager.getLogger();

    private CreditProfileCrudDaoImpl() {

    }

    public static CreditProfileCrudDaoImpl getInstance() {
        if (instance == null) {
            instance = new CreditProfileCrudDaoImpl();
            return instance;
        }
        return instance;
    }

    static {
        LoggingConfig.initializeLogger();
    }

    @Override
    public List<CreditProfile> getAll() {
        List<CreditProfile> creditProfileList = new ArrayList<>();
        logger.debug("Credit Profile Get All method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            Query query = session.createQuery("FROM CreditProfile");
            List<CreditProfile> list = query.getResultList();
            creditProfileList.addAll(list);
            session.getTransaction().commit();
            return creditProfileList;
        }
    }

    @Override
    public CreditProfile findById(int id) {
        CreditProfile creditProfile;
        logger.debug("Credit Profile Get method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            creditProfile = session.get(CreditProfile.class, id);
            session.getTransaction().commit();
            return creditProfile;
        } catch (NoResultException nre) {
            return null;
        }
    }

    @Override
    public CreditProfile findByDescription(String description) {
        CreditProfile creditProfile;
        logger.debug("Credit Profile Find By Name method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            Query query = session.createQuery("FROM CreditProfile WHERE description = '" + description + "'");
            creditProfile = (CreditProfile) query.uniqueResult();
            session.getTransaction().commit();
            return creditProfile;
        } catch (NoResultException nre) {
            return null;
        }
    }

    @Override
    public void delete(CreditProfile creditProfile) {
        logger.debug("Credit Profile Delete method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            session.remove(creditProfile);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(CreditProfile creditProfile) {
        logger.debug("Credit Profile Update method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            session.merge(creditProfile);
            session.getTransaction().commit();
        }
    }

    @Override
    public void insert(CreditProfile creditProfile) {
        logger.debug("Credit Profile Insert method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            session.save(creditProfile);
            session.getTransaction().commit();
        }
    }
}