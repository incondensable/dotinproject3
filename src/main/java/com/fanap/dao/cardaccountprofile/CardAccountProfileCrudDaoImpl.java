package com.fanap.dao.cardaccountprofile;

import com.fanap.model.card.CardAccount;
import com.fanap.model.customer.NaturalCustomers;
import com.fanap.util.HibernateUtil;
import com.fanap.util.LoggingConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class CardAccountProfileCrudDaoImpl implements CardAccountProfileCrudDao<CardAccount> {
    private static CardAccountProfileCrudDaoImpl instance = null;
    private final Logger logger = LogManager.getLogger();
    private CardAccountProfileCrudDaoImpl() {

    }
    public static CardAccountProfileCrudDaoImpl getInstance() {
        if (instance == null) {
            instance = new CardAccountProfileCrudDaoImpl();
            return instance;
        }
        return instance;
    }

    static {
        LoggingConfig.initializeLogger();
    }

    @Override
    public List<CardAccount> getAll() {
        logger.debug("Card Account Get All method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            Query query = session.createQuery("FROM CardAccount");
            return query.getResultList();
        }
    }

    @Override
    public CardAccount findById(int id) {
        logger.debug("Card Account Get method is getting Called!");
        CardAccount cardAccount;
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            cardAccount = session.get(CardAccount.class, id);
            session.getTransaction().commit();
            return cardAccount;
        }
    }

    public CardAccount findByCardAccountGroupIdAndCustomerId(int cardAccountGroupId, int customerId) {
        CardAccount cardAccount;
        logger.debug("Card Account Get By Card Account Group and Customer Id  method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            Query query =
                    session.createQuery
                            ("FROM CardAccount WHERE naturalCustomers.id='" + customerId + "'"
                                    + " AND cardAccountGroup.id='" + cardAccountGroupId + "'");
            cardAccount = (CardAccount) query.uniqueResult();
            return cardAccount;
        }
    }

    @Override
    public boolean findByCardAccountId(int cardAccountId) {
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            List<NaturalCustomers> naturalCustomers;
            session.beginTransaction();
            Query query =
                    session.createQuery
                            ("FROM CardAccount WHERE naturalCustomers.id='" + cardAccountId + "'");
            naturalCustomers = (List<NaturalCustomers>) query.getResultList();
            return naturalCustomers.size() == 0;
        }
    }

    @Override
    public void insert(CardAccount cardAccount) {
        logger.debug("Card Account Insert method is getting Called!");
        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            session.beginTransaction();
            session.save(cardAccount);
            session.getTransaction().commit();
        }
    }
}
