package com.fanap.dao.cardaccountprofile;

import java.util.List;

public interface CardAccountProfileCrudDao<T> {
    List<T> getAll();

    T findById(int id);

    T findByCardAccountGroupIdAndCustomerId(int cardAccountGroupId, int customerId);

    boolean findByCardAccountId(int cardAccountId);

    void insert(T t);
}
